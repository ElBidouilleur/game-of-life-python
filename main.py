import random
import time
import tkinter as tk

matrix = [[random.randint(0, 1) for _ in range(16)] for _ in range(20)]


def init_window(window):
    height = 15 * 32
    width = 19 * 32
    window.resizable(False, False)
    window.title("Game Of Life")
    window.geometry(str(width) + "x" + str(height) + "+0+0")


def init_game(canvas):
    i = 0
    for y in matrix:
        j = 0
        for x in y:
            if x == 0:
                canvas.create_rectangle(i * 32, j * 32, (i * 32) + 32, (j * 32) + 32, fill="#000")
            else:
                canvas.create_rectangle(i * 32, j * 32, (i * 32) + 32, (j * 32) + 32, fill="#FFF")
            j = j + 1
        i = i + 1


def update_game():
    i = -1
    for y in matrix:
        i = i + 1
        j = -1
        for x in y:
            j = j + 1
            top = None
            bottom = None
            right = None
            left = None
            left_top = None
            left_bottom = None
            right_top = None
            right_bottom = None
            if i - 1 < len(matrix):
                top = matrix[i - 1][j]
            if len(matrix) > i + 1:
                bottom = matrix[i + 1][j]
            if len(matrix[i]) > j + 1:
                right = matrix[i][j + 1]
            if j - 1 < len(matrix[i]):
                left = matrix[i][j - 1]

            if j - 1 < len(matrix[i]) and i - 1 < len(matrix):
                left_top = matrix[i - 1][j - 1]
            if j - 1 < len(matrix[i]) and len(matrix) > i + 1:
                left_bottom = matrix[i + 1][j - 1]

            if len(matrix[i]) > j + 1 and i - 1 < len(matrix):
                right_top = matrix[i - 1][j + 1]
            if len(matrix[i]) > j + 1 and len(matrix) > i + 1:
                right_bottom = matrix[i + 1][j + 1]

            nb_alive = 0
            if top == 1:
                nb_alive = nb_alive + 1
            if right == 1:
                nb_alive = nb_alive + 1
            if left == 1:
                nb_alive = nb_alive + 1
            if bottom == 1:
                nb_alive = nb_alive + 1
            if left_bottom == 1:
                nb_alive = nb_alive + 1
            if left_top == 1:
                nb_alive = nb_alive + 1
            if right_bottom == 1:
                nb_alive = nb_alive + 1
            if right_top == 1:
                nb_alive = nb_alive + 1

            if matrix[i][j] == 0:
                if nb_alive >= 3:
                    matrix[i][j] = 1
            else:
                if nb_alive != 2 and nb_alive != 3:
                    matrix[i][j] = 0

    init_game(canvas)
    window.after(250, update_game)


window = tk.Tk()

init_window(window)

canvas = tk.Canvas(window, height=16 * 32, width=20 * 32, bg="#FF0000")
canvas.pack()
init_game(canvas)
window.after(250, update_game)
window.mainloop()
